export interface ICalculatorMetadataProperties {
    /** Specifies if the default operations are shown. */
    readonly allowDefault?: boolean;

    /** Specifies if casting to a number is allowed. */
    readonly allowCastToNumber?: boolean;

    /** Specifies a scoring table. */
    readonly scores?: {
        /** Score reference. */
        readonly reference: string | boolean;

        /** Score label without markdown. */
        readonly label?: string;

        /** Score alias. */
        readonly alias?: string;

        /** Specifies the score. */
        readonly score?: number;
    }[];
}

export interface ICalculatorMetadata {
    readonly [slot: string]: ICalculatorMetadataProperties | undefined;
}
