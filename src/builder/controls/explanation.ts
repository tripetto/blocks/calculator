import { EditorOrchestrator, Forms, pgettext } from "@tripetto/builder";
import { Operation } from "../operation";

export const explanationControl = (
    editor: EditorOrchestrator<Operation>,
    explanation: string
) =>
    editor.form({
        title: pgettext("block:calculator", "Explanation"),
        controls: [new Forms.Static(explanation).markdown()],
    });
